# Before using/committing this Dockerfile into your git repository,
# make sure to replace arguments that contains [XXX] with their actual names.

FROM  node:6

ENV   NODE_ENV production

ADD   . /srv/bob5
WORKDIR  . /srv/bob5
RUN   cd /srv/bob5 ; npm install

# All product related commands should be under npm management.
# e.g. 'npm run build', 'npm test', 'npm start' etc.

# Uncomment the following line if this product requires building procedures.
# RUN     npm run build;

# Starting product
RUN   npm install -g pm2 && \
      pm2 startup

CMD   pm2 start --name "bob5" --no-daemon index.js

# By default all applications run on port 3000 in the container.
# It is not recommended to change the port.
# If you'd like the application to listen on a different port,
# use `-p` to expose port 3000 in container to another port on host.
EXPOSE 3000