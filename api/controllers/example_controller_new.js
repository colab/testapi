"use strict";
var controller = module.exports;

/**
 * The Request Object is a standard node http.IncomingMessage, with a swagger attribute
 * @typedef {Object} RequestObject 
 * @property {SwaggerAddon} swagger 
 * @property {Object}       headers - Object with header names as keys and values as values.
 * @property {string}       httpVersion - String containing http version (probably 1.1). 
 * @property {string}       method - String containing method type (probably "GET" or "POST"). 
 * @property {string}       url - String containing the raw URL from the raw HTTP request. (example: "/path1/path2/file.txt")
 */

/**
 * The structure of req.swagger 
 * @typedef {Object} SwaggerAddon 
 * @property {string}   apiPath             - The API's path (The key used in the paths object for the corresponding API). 
 * @property {Object}   path                - The corresponding path in the Swagger object that the request maps to. 
 * @property {Object}   operation           - The corresponding operation in the API Declaration that the request maps to. 
 * @property {Object[]} operationParameters - The computed parameters for this operation. 
 * @property {string[]} operationPath       - The path to the operation in string array format. 
 * @property {Object}   params              - For each of the request parameters defined in your Swagger document, its path, its schema and its processed value. (In the event the value needs coercion and it cannot be converted, the value property will be the original value provided.) 
 * @property {Object[]} security            - The computed security for this request 
 * @property {Object}   swaggerObject       - The Swagger full swagger Document Object 
 */

/**
 * Operation get: Returns 'Hello' to the caller
 * @param {RequestObject}  req - The Request Object 
 * @param {Object}         res - The Response Object (see https://nodejs.org/api/http.html#http_class_http_serverresponse) 
 * req.swagger.params parameters expected:
 *   name(Optional)
 */
controller["get"] = function(req,res) {
	//YOUR CODE GOES HERE
	res.statusCode = 501;
	res.end('{"error":"Not implemented yet."}');
};